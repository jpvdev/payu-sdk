export const oddEvent = (value: number): boolean => value % 2 === 0;
