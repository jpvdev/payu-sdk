import oddEven from './oddEven';

describe('test oddEven', (): void => {
  test('odd', (): void => {
    const resp: boolean = oddEven(1);
    expect(resp).toBe(false);
  });

  test('even', (): void => {
    const resp: boolean = oddEven(2);
    expect(resp).toBe(true);
  });
});
