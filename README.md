# PAYU SDK JPVDEV

Package to use payu payments

## FOLDER STRUCTURE

Explanation of hierarchies in files and layers.

```
payu-sdk-jpvdev/
├── src/
|   ├── constants/                  # Folder with all constants
│   ├── services/                   # Folder with all the services
│   └── index.ts                    # Main point entry file
```

## ENVIRONMENT

- Yarn (any version)
- Node (v14.17.2)

## LOCAL CONFIGURATION

Explanation of commands to execute.

1.  ```bash
    yarn
    ```
2.  ```bash
    yarn build
    ```
3.  ```bash
    yarn link
    ```
